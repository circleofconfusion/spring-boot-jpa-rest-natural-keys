# Test of Natural Keys with Spring Boot and Hibernate

Natural keys are unique identifiers that have some sort of business logic. In 
this case we have a user table. Since the usernames need to be unique anyway,
the username field is perfectly usable as a Primary Key. The Config table 
has a frontend-generated unique ID, so once again, a natural key is
appropriate -- just use the UID.

## Run the Test Server

No need to explicitly compile. To run, just execute:

    mvn spring-boot:run

from the root directory.

## REST Interface

There are two REST endpoints: `/configs` and `/users`.

To test the repositories, first create a user by sending a POST with this
message body to `http://localhost:8080/users`:

    {"username":"someuser","firstName":"John","lastName":"Doe"}

Then send POSTs to `http://localhost:8080/configs` with the message bodies:

    {"uid":"dashboard-1","type":"dashboard","user":"/users/someuser"}

    {"uid":"widget-1","type":"widget","user":"/users/someuser"}

Subsequent GET requests to these endpoints will then return arrays of stored data.

GETting `/users/someuser` will return information for just one user.

GETting `/configs/dashboard-1` will return just the dashboard-1 record.

## Notes

There are several key things that make all this work.

1. Hibernate has a `@NaturalId` annotation that when used in combination with the 
`@Id` annotation allows for String keys that may be referenced as foreign keys.
2. The user field in Config.java needs a `@ManyToOne` annotation to store one user.
No other annotation is needed since Hibernate just figures out which of the User
fields is the primary key.
3. In the message body, the user field is a reference to a record in the User
table. As such, a URI reference to that record is all that is needed to POST a
new configuration.
