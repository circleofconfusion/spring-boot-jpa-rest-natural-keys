package com.knudsen.springbootdatarest;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ConfigRepository extends PagingAndSortingRepository<Config, String>{
    Config findByUid(@Param("uid") String uid);
    Config findByUser(@Param("username") User user);
}
